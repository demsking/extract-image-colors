'use strict'

const path = require('path')
const color = require('../../lib/color')

/* global describe it expect */

describe('lib/color', () => {
  describe('extract(filename)', () => {
    ['jpg', 'png', 'gif', 'svg'].forEach((ext) => {
      const filename = path.join(__dirname, '..', 'fixtures', `thumb.${ext}`)

      it(`should successfully extract colors for ${ext} image`, () => {
        return color.extract(filename)
          .then((colors) => expect(colors instanceof Array).toBeTruthy())
      })
    })

    it('should throw an error for an unexisting image', (done) => {
      color.extract('/unexisting/image.png')
        .then(() => done(new Error('should throw an error for an unexisting image')))
        .catch(() => done())
    })
  })
})
