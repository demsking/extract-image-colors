'use strict'

const path = require('path')
const { spawn } = require('child_process')

/* global describe it expect */

describe('bin/cli', () => {
  describe('image-colors', () => {
    it('should failed with missing filename arg', (done) => {
      const cli = spawn('node', [
        'bin/cli.js'
      ])

      cli.stdout.on('data', () => {
        done(new Error('should failed with missing filename arg'))
      })

      cli.stderr.on('data', () => {
        done()
      })
    })

    it('should successfully extract colors', (done) => {
      const cli = spawn('node', [
        'bin/cli.js', path.join(__dirname, '..', 'fixtures', 'thumb.jpg')
      ])

      cli.stdout.on('data', (output) => {
        expect(output.toString().length > 0).toBeTruthy()
        done()
      })

      cli.stderr.on('data', (output) => {
        done(new Error(output.toString()))
      })
    })

    it('should failed with an unexisting image', (done) => {
      const cli = spawn('node', [
        'bin/cli.js', '/unexisting/image.png'
      ])

      cli.stdout.on('data', () => {
        done(new Error('should failed with an unexisting image'))
      })

      cli.stderr.on('data', () => {
        done()
      })
    })
  })
})
