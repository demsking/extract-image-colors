#!/usr/bin/env node

'use strict'

if (process.argv.length < 3) {
  process.stderr.write('Missing input image filename\n')
  process.exit(1)
}

const color = require('../lib/color')

process.argv.slice(2).forEach((filename) => {
  return color.extract(filename)
    .then((colors) => JSON.stringify(colors) + '\n')
    .then((output) => process.stdout.write(output))
    .catch(({ message }) => {
      process.stderr.write(message + '\n')
      process.exit(2)
    })
})
