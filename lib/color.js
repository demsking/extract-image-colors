'use strict'

const getColors = require('get-image-colors')

module.exports.extract = (filename) => getColors(filename)
  .then((colors) => colors.map(({ _rgb }) => _rgb))
  .then((colors) => colors.map(([ r, g, b ]) => [ r, g, b ]))
