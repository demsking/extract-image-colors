# Extract Image Colors

A command line tool to extract colors from an image

[![npm](https://img.shields.io/npm/v/extract-image-colors.svg)](https://www.npmjs.com/package/extract-image-colors) [![Build status](https://gitlab.com/demsking/extract-image-colors/badges/master/build.svg)](https://gitlab.com/demsking/extract-image-colors/commits/master) [![Test coverage](https://gitlab.com/demsking/extract-image-colors/badges/master/coverage.svg)](https://gitlab.com/demsking/extract-image-colors/pipelines)

## Install

```sh
npm install -g extract-image-colors
```

## Features

- Support PNG Images
- Support SVG Images
- Support GIF Images
- Support JPEG Images

## Usage

**extract colors for an image**

```sh
extract-image-colors path/to/image.jpg
# output:
# [[144,154,63],[224,228,221],[68,54,21],[211,215,141],[68,84,12]]
```

**with multiple images**

```sh
extract-image-colors path/to/black-mirror.jpg path/to/got.png
# output:
# [[144,154,63],[224,228,221],[68,54,21],[211,215,141],[68,84,12]]
# [[234,240,233],[41,41,40],[181,58,53],[133,148,155],[134,126,112]]
```

## License

Under the MIT license. See [LICENSE](https://gitlab.com/demsking/extract-image-colors/blob/master/LICENSE) file for more details.
